package main;

import java.util.List;
import java.util.stream.Collectors;

public class Ejecutable {
    public static void main(String args[]) {
        List<Integer> calificaciones = List.of(10, 10, 9, 4, 5, 6, 7, 8, 9, 10);

        System.out.println("Ejemplo de busqueda de valores dentro de un Stream.");
        System.out.println("Lista de calificaciones original: ");
        System.out.println(calificaciones);
        boolean result = calificaciones.stream().anyMatch( calificacion -> calificacion < 6);
        System.out.println("Resultado de la busqueda: ");
        System.out.println(result);
        //
        System.out.println("Ejemplo con filter: ");
        result = calificaciones.stream().filter( calificacion -> calificacion < 6).count() > 0;
        System.out.println(result);
    }
}
